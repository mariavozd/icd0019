package exceptions.numbers;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Properties;

public class NumberConverter {

    private Properties properties;
    private String lang;

    public NumberConverter(String lang) {

        String filePath = String.format(
                "src/exceptions/numbers/numbers_%s.properties", lang);

        this.properties = new Properties();
        FileInputStream is = null;

        try {
            is = new FileInputStream(filePath);

            InputStreamReader reader = new InputStreamReader(
                    is, Charset.forName("UTF-8"));
            this.properties.load(reader);

        } catch (FileNotFoundException e) {
            throw new MissingLanguageFileException();

        } catch (IllegalArgumentException e) {
            throw new BrokenLanguageFileException();

        } catch (IOException e){
            System.out.println("Caught.");

        } finally {
            try {
                is.close();

            } catch (IOException e) {
                System.out.println("Done");

            } catch (NullPointerException e) {
                throw new MissingLanguageFileException();
            }


        }

    }

    public String numberInWords(Integer number) {

        try {
            if (properties.getProperty(String.valueOf(0)).equals("zero")) {
                this.lang = "en";
            }
            else if (properties.getProperty(String.valueOf(0)).equals("null")) {
                this.lang = "et";
            }
        } catch (NullPointerException e) {
            throw new MissingTranslationException();
        }


        if (0 <= number && number <= 10) {
            return properties.getProperty(String.valueOf(number));
        }
        else {
            if (this.lang.equals("et") && number == 20) {
                return properties.getProperty(String.valueOf(2)) +
                        properties.getProperty("tens-suffix");

            }

            else if (this.lang.equals("et") && number >= 11 ) {
                return properties.getProperty(String.valueOf(number - 10)) +
                        properties.getProperty("teen");
            }

            else if (this.lang.equals("en") && number - 10 == 4 || number - 10 == 6 || number - 10 == 9 ||
            number - 10 == 7) {
                return properties.getProperty(String.valueOf(number - 10)) +
                        properties.getProperty("teen");
            }

            else if (this.lang.equals("en") && number > 10) {
                return properties.getProperty(String.valueOf(number));
            }

        }
        return null;
    }

}
