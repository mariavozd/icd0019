package inheritance.analyser;

public class DifferentiatedTaxSalesAnalyser extends AbstractSalesAnalyser {

    private Double mTAX_RATE = 0.1;


    public DifferentiatedTaxSalesAnalyser(SalesRecord[] records) {
        super(records);
    }

    @Override
    public Double getTotalSales() {
        Double sales = 0.0;

        for (SalesRecord record : newRecords) {
            if (record.hasReducedRate()) {
                sales += record.getProductPrice() * record.getItemsSold() / (1 + mTAX_RATE);

            } else {
                sales += record.getProductPrice() * record.getItemsSold() / (1 + TAX_RATE);
            }
        }

        return sales;
    }

    @Override
    public Double getTotalSalesByProductId(String id) {
        Double salesID = 0.0;

        for (SalesRecord record : newRecords) {
            if (record.getProductId().equals(id)){

                if(record.hasReducedRate()) {
                    salesID += record.getProductPrice() * record.getItemsSold() / (1 + mTAX_RATE);
                }
                salesID += record.getProductPrice() * record.getItemsSold() / (1 + TAX_RATE);
            }

        }
        return salesID;
    }

    public String getIdOfItemWithLargestTotalSales() {
        throw new RuntimeException("not implemented yet");
    }


}
