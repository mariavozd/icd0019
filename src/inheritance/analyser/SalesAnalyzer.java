package inheritance.analyser;

public class SalesAnalyzer {

    protected final SalesRecord[] newRecords;

    public SalesAnalyzer(SalesRecord[] records) {
        this.newRecords = records;
    }

    public Double getTotalSales() {
        Double sales = 0.0;

        for (SalesRecord record : newRecords) {
            sales += record.getProductPrice() * record.getItemsSold();
        }

        return sales;
    }

    public Double getTotalSalesByProductId(String id) {
        Double salesById = 0.0;

        for (SalesRecord record : newRecords) {
            if (record.getProductId().equals(id)) {
                salesById += record.getProductPrice() * record.getItemsSold();
            }
        }

        return salesById;
    }

    public String getIdOfMostPopularItem() {
        throw new RuntimeException("not implemented yet");
    }

    public String getIdOfItemWithLargestTotalSales() {
        throw new RuntimeException("not implemented yet");
    }
}
