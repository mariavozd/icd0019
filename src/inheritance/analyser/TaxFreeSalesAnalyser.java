package inheritance.analyser;

public class TaxFreeSalesAnalyser extends AbstractSalesAnalyser {


    public TaxFreeSalesAnalyser(SalesRecord[] records) {
        super(records);
        this.TAX_RATE = 0.0;
    }

}
