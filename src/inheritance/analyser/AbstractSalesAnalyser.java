package inheritance.analyser;

public abstract class AbstractSalesAnalyser {

    protected SalesRecord[] newRecords;
    protected Double TAX_RATE;


    public AbstractSalesAnalyser(SalesRecord[] records) {
        this.newRecords = records;
        this.TAX_RATE = 0.2;

    }

    public Double getTotalSales() {
        Double sales = 0.0;

        for (SalesRecord record : newRecords) {
            sales += record.getProductPrice() * record.getItemsSold() / (1 + TAX_RATE);
        }

        return sales;
    }

    public Double getTotalSalesByProductId(String id) {
        Double salesById = 0.0;

        for (SalesRecord record : newRecords) {
            if (record.getProductId().equals(id)) {
                salesById += record.getProductPrice() * record.getItemsSold() / (1 + TAX_RATE);
            }
        }

        return salesById;
    }

    public String getIdOfMostPopularItem() {
        int current = newRecords[0].getItemsSold();

        for (SalesRecord record : newRecords) {
            if (record.getItemsSold() > current) {
                current = record.getItemsSold();
            }

        }
        for (SalesRecord record : newRecords) {
            if (record.getItemsSold().equals(current)) {
                return record.getProductId();
            }

        }
        return null;
    }

    public String getIdOfItemWithLargestTotalSales() {
        int count = 1;
        int tempCount;

        String mostFrequent = newRecords[0].getProductId();
        String temp;

        for (int i = 0; i < (newRecords.length - 1); i++) {
            temp = newRecords[i].getProductId();
            tempCount = 0;

            for (int j = 1; j < newRecords.length; j++) {
                if (temp.equals(newRecords[j].getProductId()))
                    tempCount++;

            }if (tempCount > count) {
                mostFrequent = temp;
                count = tempCount;
            }
        }
        return mostFrequent;
    }

}
