package generics.cart;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart<T> {

    private List<T> elements = new ArrayList<>();

    public void add(T item) {
        elements.add(item);
    }

    public void removeById(String id) {

        List<T> toBeRemoved = addItemsToList(id);  // To avoid ConcurrentModificationException.
        elements.removeAll(toBeRemoved);

    }

    private boolean containsId(String id) {

        for (T element : elements) {
            if (((CartItem) element).getId().equals(id)) {
                return true;
            }
        }
        return false;
    }

    public Double getTotal() {
        Double total = 0.0;

        for (T element : elements) {
            total += ((CartItem) element).getPrice();
        }
        return total;

    }

    public List<T> addItemsToList(String id) {         // Adds item(s) to a list if the id matches.
        List<T> itemsToAdd = new ArrayList<>();

        for (T element : elements) {
            if (((CartItem) element).getId().equals(id)) {
                itemsToAdd.add(element);
            }
        }
        return itemsToAdd;
    }

    public void increaseQuantity(String id) {

        List<T> itemsToAdd = addItemsToList(id);
        elements.addAll(itemsToAdd);
    }

    public void applyDiscountPercentage (Double discount) { // override or extend getall???
        for (T element : elements) {
            Double price = ((CartItem)element).getPrice();
        }
    }

    public void cancelDiscounts() {
        throw new RuntimeException("not implemented yet");
    }

    public void addAll(List<? extends T> items) {
        for (T item : items) {
            add(item);
        }
    }
}
