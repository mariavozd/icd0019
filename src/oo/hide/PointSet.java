package oo.hide;

public class PointSet {

    private Point[] arr;
    private Integer start = 0;

    public PointSet(Integer initialCapacity) {
        arr = new Point[initialCapacity];
    }

    public PointSet() {
        arr = new Point[100];
    }

    public void add(Point point) {
        boolean duplicate = false;
        int size = size();

        if (start.equals(0)) {
            arr[start] = point;
            start++;

        } else if (start > 0) {

            for (int i=0; i < size; i++) {
                if (arr[i].equals(point)) {
                    duplicate = true;
                }
            }

            if (!(duplicate)) {
                arr[start] = point;
                start++;
            }

        }

    }


    public Integer size() {
        int size = 0;

        for (Point point : arr) {
            if (point != null) {
                size ++;
            }
        }
        return size;
    }

    public boolean contains(Point p) {
        Integer size = size();

        for (int i = 0; i < size; i++) {
            if (arr[i].equals(p)) {
                return true;
            }

        }

        return false;
    }

    @Override
    public String toString() {
        String result = "";
        int lastElement = size() - 1;

        for (Point point : arr) {
            if (point != null && point != arr[lastElement]) {
                result += (point + ", ");
            } else if (point != null && point.equals(arr[lastElement])) {
                result += point;
            }

        }
        return result;
    }
}
