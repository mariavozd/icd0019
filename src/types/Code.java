package types;

public class Code {

    public static void main(String[] args) {

        Integer[] numbers = new Integer [] {1, 3, -2, 9};
        System.out.println(sum(numbers));
        System.out.println(average(numbers));
        System.out.println(minimumElement(numbers));
        System.out.println(asString(numbers));
        String s = "a9b2";
        System.out.println(squareDigits(s));


    }

    public static Integer sum(Integer[] numbers) {
        Integer sum = 0;
        for (Integer number : numbers) {
            sum += number;
        }
        return sum;
    }

    public static Double average(Integer[] numbers) {
        Double sum = 0.0;
        for (Integer number : numbers) {
           sum += number;
        }
        return sum / numbers.length;
    }

    public static Integer minimumElement(Integer[] integers) {
        if (integers.length == 0) {
            return null;
        }

        Integer min = integers[0];

        for (Integer each : integers) {

            if (each < min) {
                min = each;
            }
        }

        return min;

    }

    public static String asString(Integer[] elements) {
        String result = "";

        for (Integer element : elements) {
            if (element.equals(elements[elements.length - 1])) {
                result += element;
            } else
                result += (element + ", "); {
            }
        }
        return result;
    }

    public static String squareDigits(String s) {
        String result = "";
        Integer number;

        for (int i = 0; i < s.length(); i++) {
            if (Character.isDigit(s.charAt(i))) {
                number = Character.getNumericValue(s.charAt(i));
                result += number * number;
            } else
                result += Character.toString(s.charAt(i)); {

            }
        }
        return result;

    }


}
