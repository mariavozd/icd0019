package junit;

public class Code {


    public static boolean isSpecial(Integer number) {

        return (number % 11 == 1 || number % 11 == 0);
        
    }


    public static Integer longestStreak(String input) {
        if (input.length() == 0) {
            return 0;
        }

        // aaabc

        String[] characters = input.split("");
        String lastSymbol = characters[0];

        int longestStreak = 0;
        int streakLength = 0;

        for (String currentSymbol : characters) {

            if (currentSymbol.equals(lastSymbol)) {
                streakLength++;
            } else {
                streakLength = 1;
            }

            if (longestStreak < streakLength) {
                longestStreak = streakLength;
            }

            lastSymbol = currentSymbol;
        }

        return longestStreak;
    }


    public static Integer[] removeDuplicates(Integer[] input) {

        Integer arraySize = input.length;
        Integer shift;
        Integer newArrSize;



        for (int i = 0; i < arraySize; i++ ) {
            for (int nextNum = i + 1; nextNum < arraySize; nextNum++) {
                if (input[i].equals(input[nextNum])) {
                    shift = nextNum;
                    for (newArrSize = nextNum + 1; newArrSize < arraySize; newArrSize++, shift++) {
                        input[shift] = input[newArrSize];
                    }
                    arraySize--;
                    nextNum--;
                }
            }

        }

        Integer[] newArray = new Integer[arraySize];
        for (int i = 0; i < arraySize; i++) {
            newArray[i] = input[i];
        }

        return newArray;

    }


    public static Integer sumIgnoringDuplicates(Integer[] integers) {

        Integer[] noDuplicates = removeDuplicates(integers);
        Integer result = 0;

        for (Integer num : noDuplicates) {
             result += num;
        }

        return result;
    }

}
