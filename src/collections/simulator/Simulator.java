package collections.simulator;

import java.util.*;

public class Simulator {



    public Map<HandType, Double> calculateProbabilities() {
        Map<HandType, Double> probability = new HashMap<>();
        List<Integer> cardsProbability = new ArrayList<>();

        for (int i = 0; i < 500000 ; i++) {
            cardsProbability.add(FindFirstPair()); // Adds 1 or 0 to the list.
        }

        Double count = 0.0;
        for (Integer integer : cardsProbability) {
            count += integer;
        }

        probability.put(HandType.PAIR, count * 100 / cardsProbability.size());

        return probability;
    }



    public int FindFirstPair() {
        List<Card>cardDeck = packOfCards();
        Hand hand = new Hand(); // Make a new hand every time the function is repeated.
        Random rand = new Random();

        for (int i = 0; i < 5; i++) {
            Card randomCard = cardDeck.get(rand.nextInt(cardDeck.size()));  // Get a random card from card deck.
            hand.addCard(randomCard); // Add card to the hand.
            cardDeck.remove(randomCard); // Remove card from the deck.
        }

        if (hand.isOnePair()) {
            return 1; // If pair is found, return 1.
        }
        return 0;
    }


    public List<Card> packOfCards() {
        List<Card>cardDeck = new ArrayList<>();

        for (Card.CardValue value : Card.CardValue.values()) {
            for (Card.CardSuit cardSuit : Card.CardSuit.values()) {
                Card card = new Card(value, cardSuit);
                cardDeck.add(card);
            }
        }

        Collections.shuffle(cardDeck); // Shuffle the card deck.
        return cardDeck;
    }

}
