package collections.simulator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Hand {

    public List<Card> hand = new ArrayList<>();

    public void addCard(Card card) {
        hand.add(card);

    }

    @Override
    public String toString() {
        String listString = "";

        for (Card card : hand) {
            if (card.equals(hand.get(0))) {
                listString += card.toString();
            }
            listString += ", " + card.toString();
        }

        return listString;
    }


    public boolean isOnePair() {
        int count = 0;
        boolean pair = false;

        Set<Card.CardValue> cardVals = new HashSet<>();

        for (Card card : hand) {
            if (!cardVals.add(card.value)) {
                count++;
                pair = true;
            }

        }
        if (count > 1) {
            pair = false;
        }

        return pair;
    }

    public boolean isTwoPairs() {
        int count = 0;
        boolean pair = false;

        Set<Card.CardValue> cardVals = new HashSet<>();

        for (Card card : hand) {
            if (!cardVals.add(card.value)) {
                count++;
                pair = true;
            }

        }
        if (count > 2) {
            pair = false;
        }

        return pair;
    }

    public boolean isTrips() {
        throw new RuntimeException("not implemented yet");
    }

    public boolean isFullHouse() {
        throw new RuntimeException("not implemented yet");
    }

}
