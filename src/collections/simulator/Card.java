package collections.simulator;

public class Card implements Comparable<Card> {

    public enum CardValue { s2, s3, s4, s5, s6, s7, s8, s9, s10, J, Q, K, A }

    public enum CardSuit { C, D, H, S }

    public CardValue value;
    public CardSuit suit;

    public Card(CardValue value, CardSuit suit) {
        this.value = value;
        this.suit = suit;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Card && ((Card) obj).value == value && ((Card) obj).suit == suit) {
            return true;
        }
        return false;
    }

    @Override
    public int compareTo(Card other) {
        int firstIndex;
        int secondIndex;
        int result = 0;

        for (int i = 0; i < CardValue.values().length ; i++) {
            if (value.equals(CardValue.values()[i])) {
                firstIndex = i;
                result += firstIndex;
            }
            if (other.value.equals(CardValue.values()[i])) {
                secondIndex = i;
                result -= secondIndex;

            }
        }
        return result;
    }

    @Override
    public String toString() {
        return String.format("(%s, %s)", value, suit);
    }
}
